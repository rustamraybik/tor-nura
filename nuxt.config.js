export default {
  head: {
    title: 'online-tor',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  plugins: [
    { src: '~/plugins/axios' },
    { src: '~/plugins/dialogModal' },
    { src: '~/plugins/vuelayers.js', ssr: false },
    { src: '~/plugins/vCalendar', ssr: false },
  ],
  components: true,
  loading: { color: '#23C1A5' },
  buildModules: [
    ['@nuxtjs/vuetify', {
      theme: {
        themes: {
          light: {
            primary: '#23C1A5',
          }
        }
      }
    }],
  ],
  modules: [
    '@nuxtjs/axios',
    '~/shared/vueLayers',
  ],
  axios: {
    baseURL: process.env.API_URL,
    headers: {
      'Access-Control-Allow-Origin': '*',
      Accept: 'application/json',
    },
  },
  css: [
    'vuetify/dist/vuetify.css',
  ],
  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/
        })
      }
    }
  }
}
