export const state = () => ({

})


export const getters = {

}


export const mutations = {

}


export const actions = {
    async loadVu23({ }, payload = {}) {
        const { page, count, search, vu23Status, date, applicationStatus } = payload;
        return await this.$axios.get('/vu23' +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : '') +
            (vu23Status ? `&vu23Status=${vu23Status}` : '') +
            (applicationStatus ? `&applicationStatus=${applicationStatus}` : '') +
            (date ? `&date=${date}` : ''));
    },
    
    async signVu23({ }, payload = {}) {
        const { vu23_id, requestData } = payload;
        return await this.$axios.$post(`/vu23/${vu23_id}/sign`, requestData);
    },

    async downloadVu23Doc({ }, payload = {}) {
        const { vu23_id, file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/vu23/${vu23_id}/document/${file_id}/download` +
            (format ? `?format=${format}` : ''), option);
    },
}