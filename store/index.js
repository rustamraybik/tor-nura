const cookieparser = process.server ? require('cookieparser') : undefined

export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { req }) {
    let token = null
    if (req && req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      token = parsed.token
      if (token) {
        try {
          await dispatch('auth/loadAuthUser')
          commit('auth/SET_LOGIN', true)
          commit('auth/SET_TOKEN', token)
        } catch (error) {
          commit('auth/SET_LOGOUT')
          return error
        }
      }
    }
  },
}
