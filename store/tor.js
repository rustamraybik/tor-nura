const getDefaultState = () => ({
    tors: { data: [], meta: {} },
})

export const state = getDefaultState;

export const getters = {
    getTorForMap: (state) => {
        return state.tors.data.map((tor) => {
            const mapResult = {
                id: tor.id,
                code: tor.station.code,
                station: tor.station.name,
                name: tor.station.name,
                coordinates: [parseFloat(tor.longitude), parseFloat(tor.latitude),],
            };
            return mapResult;
        });
    },

    getTorList: (state) => {
        const result = Object.assign({}, state.tors);

        result.data = state.tors.data.map((tor) => {
            const mapResult = {
                id: tor.id,
                tor_name: tor.tor_name,
                depot: tor.depot.name,
                station: tor.station.name,
                latitude: tor.latitude,
                longitude: tor.longitude
            };
            Object.defineProperty(mapResult, "station_id", {
                value: tor.station.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "depot_id", {
                value: tor.depot.id,
                enumerable: false,
            });
            return mapResult;
        });

        return result;
    },
}


export const mutations = {
    SET_TORS(state, payload) {
        state.tors = payload;
    },
    resetState: (state) => {
        Object.assign(state, getDefaultState())
    },
}


export const actions = {
    resetState({ commit }) {
        commit('resetState');
    },

    async loadTors({ commit }, payload = {}) {
        const { page, count, search, is_paginate } = payload;
        const response = await this.$axios.get('/dictionary/tor' +
            (Object.keys(payload).length ? '?' : '') +
            (is_paginate ? `&is_paginate=1` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));

        commit('SET_TORS', response.data);
    },

    async createTor({ }, payload = {}) {

        await this.$axios.post(`/admin/tor/store`, payload);
    },

    async updateTor({ }, payload = {}) {
        const { id } = payload;
        await this.$axios.post(`/admin/tor/${id}/update`, payload);
    },

    async deleteTor({ }, payload = {}) {
        const { id } = payload;
        await this.$axios.delete(`/admin/tor/${id}`);
    },
}