export const actions = {
    async sendCallbackForm({ }, payload = {}) {
        await this.$axios.post('/callback/store', payload)
    },
}
