const getDefaultState = () => ({
    isShowInfo: false,
    isCreateForm: false,
    isCreateEquip: false,
    isEdit: false,
    equipType: null,
    id: null,
})

export const state = getDefaultState;

export const getters = {
    getIsCreateEquip: (state) => {
        return state.isCreateEquip
    },
    getIsEdit: (state) => {
        return state.isEdit
    },
    getIsCreateForm: (state) => {
        return state.isCreateForm
    },
    getIsShowInfo: (state) => {
        return state.isShowInfo
    },
    getEquipType: (state) => {
        return state.equipType
    },
    getId: (state) => {
        return state.id
    },
}

export const mutations = {
    SET_IS_CREATE_FORM(state, payloads) {
        state.isCreateForm = payloads
    },
    SET_IS_CREATE_EQUIP(state, payloads) {
        state.isCreateEquip = payloads;
    },
    SET_IS_EDIT(state, payloads) {
        state.isEdit = payloads;
    },
    SET_EQUIP_TYPE(state, payloads) {
        state.equipType = payloads;
    },
    SET_SHOW_INFO(state, payloads) {
        state.isShowInfo = payloads;
    },
    SET_ID(state, payloads) {
        state.id = payloads;
    },
    resetState: (state) => {
        Object.assign(state, getDefaultState())
    },
}

export const actions = {
    openCreateForm({ commit }, payload) {
      commit('SET_IS_CREATE_FORM', payload);
    },
    editItem({ commit }, payload) {
        const { isEdit, id } = payload;
        commit('SET_IS_EDIT', isEdit);
        commit('SET_ID', id);
    },
    openCreateEquip({ commit }, payload) {
        const { isOpen, equipType, isEdit } = payload;
        commit('SET_IS_CREATE_EQUIP', isOpen);
        commit('SET_EQUIP_TYPE', equipType);
        commit('SET_IS_EDIT', isEdit);
    },
    showInfo({ commit }, payload) {
        const { isOpen, id } = payload;
        commit('SET_SHOW_INFO', isOpen);
        commit('SET_ID', id);
    },
    resetState({ commit }) {
        commit('resetState');
    },
}
