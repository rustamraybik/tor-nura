export const state = () => ({
    users_table_list: {
        data: [],
        meta: {},
    },
    test_signs_table_list: {
        data: [],
        meta: {}
    },
    docs_table_list: {
        data: [],
        meta: {}
    }
})

export const getters = {
    getUsersTableList: (state) => {
        return state.users_table_list
    },
    getTestSignsTableList: (state) => {
        return state.test_signs_table_list
    },
    getDocsTableList: (state) => {
        return state.docs_table_list
    }
}

export const mutations = {
    SET_USERS_TABLE_LIST(state, payload) {
        state.users_table_list.data = payload.data
        state.users_table_list.meta = payload.meta
    },
    SET_TEST_SIGNS_TABLE_LIST(state, payload) {
        state.test_signs_table_list.data = payload.data;
        state.test_signs_table_list.meta = payload.meta;
    },
    SET_DOCS_TABLE_LIST(state, payload) {
        state.docs_table_list.data = payload.data;
        state.docs_table_list.meta = payload.meta;
    },
}

export const actions = {
    async loadUsers({ commit }, payload = {}) {
        const { page, count, search, role_id, is_ban } = payload;

        const response = await this.$axios.get('/admin/user' +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (is_ban ? `&is_ban=${is_ban}` : '') +
            (role_id ? `&role_id=${role_id}` : '') +
            (search ? `&search=${search}` : '')
        )

        const parsedData = response.data.data.map((user) => {
            const mapResult = {
                org_full_name: user.organization.full_name,
                org_itn: user.organization.itn,
                user_name: user.name,
                user_email: user.email,
                user_role: user.role.name,
                user_depot: user.depot?.name || '-',
                user_station: user.station?.name || '-',
                created_at: user.created_at,
                updated_at: user.updated_at,
            };

            Object.defineProperty(mapResult, "id", {
                value: user.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "role_id", {
                value: user.role.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "depot_id", {
                value: user.depot?.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "station_id", {
                value: user.station?.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "ban", {
                value: user.ban,
                enumerable: false,
            });
            return mapResult;
        });

        response.data.data = parsedData;
        commit('SET_USERS_TABLE_LIST', response.data);
    },

    async addUser({ }, payload = {}) {
        return await this.$axios.post('/admin/user/store', payload)
    },

    async banUser({ }, payload = {}) {
        const { user_id, } = payload
        return await this.$axios.post(`/admin/user/${user_id}/ban`, payload)
    },

    async updateUser({ }, payload = {}) {
        const { id } = payload
        return await this.$axios.post(`/user/${id}/update`, payload)
    },

    async loadUserDocument({ commit }, payload = {}) {
        const { page, count, type_id } = payload

        const response = await this.$axios.get(`/user/document` +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (type_id ? `&type_id=${type_id}` : ''));

        const parsedData = response.data.data.map((doc) => {
            let mapResult = {
                name: doc.name,
                signing_date: doc.signing_date,
                is_sign_success: doc.is_sign_success
            };

            Object.defineProperty(mapResult, "file_id", {
                value: doc.file_id,
                enumerable: false,
            });

            return mapResult;
        });

        const parsedDocsData = response.data.data.map((doc) => {
            let mapResult = {
                name: doc.name,
                created_at: doc.created_at,
            };

            Object.defineProperty(mapResult, "id", {
                value: doc.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "file_id", {
                value: doc.file_id,
                enumerable: false,
            });

            return mapResult;
        });

        if (type_id === 1) {
            commit('SET_TEST_SIGNS_TABLE_LIST', { data: parsedData, meta: response.data.meta });
        }
        if (type_id === 2) {
            commit('SET_DOCS_TABLE_LIST', { data: parsedDocsData, meta: response.data.meta });
        }
    },

    async downloadUserDocument({ commit }, payload = {}) {
        const { file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };

        return await this.$axios.$get(`/user/document/download` +
            (Object.keys(payload).length ? '?' : '') +
            (file_id ? `&file_id=${file_id}` : '') +
            (format ? `&format=${format}` : ''), option);
    },

    async uploadUserDocument({ commit }, payload) {
        let formData = new FormData();
        formData.append('file', payload[0]);
        return await this.$axios.$post(`/user/document/upload`,
            formData
        );
    },

    async deleteDocument({ commit }, payload) {
        const { file_id } = payload;
        return await this.$axios.$delete(`/user/document/${file_id}/delete`);
    },

    async signUserDocument({ commit }, payload = {}) {
        const { requestData } = payload;
        return await this.$axios.$post(`/user/document/sign`, requestData);
    },

    async failSign({ commit }, payload = {}) {
        return await this.$axios.$post(`/user/document/sign/fail`);
    }

}
