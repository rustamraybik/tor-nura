const Cookie = process.client ? require('js-cookie') : undefined

export const state = () => ({
  logged: false,
  token: '',
  user: {
    id: '',
    name: '',
    email: '',
    phone: '',
    organization: null,
    role_id: '',
    role_name: '',
    station: null,
    depot: null,
  }
})

export const getters = {
  getUser: (state) => {
    return state.user
  },

  getUserLogged: (state) => {
    return !!state.logged && !!state.token && !!state.user.name && !!state.user.email
  },

  getRolesFilter: (state) => {
    return state.roles_filter
  }
}

export const mutations = {
  SET_LOGIN(state) {
    if (process.client) {
      Cookie.set('logged', true)
    }
    state.logged = true
  },
  SET_TOKEN(state, token) {
    if (process.client) {
      Cookie.set('token', token)
    }
    state.token = token
  },
  SET_LOGOUT(state) {
    if (process.client) {
      Cookie.remove('logged')
      Cookie.remove('token')
    }
    state.logged = false
    state.token = false
  },
  SET_USER(state, payload) {
    state.user.id = payload.id
    state.user.name = payload.name
    state.user.email = payload.email
    state.user.role_id = payload.role.id
    state.user.role_name = payload.role.name
    state.user.phone = payload.phone
    state.user.organization = payload.organization
    state.user.station = payload.station
    state.user.depot = payload.depot
  },
  SET_FILTER_ROLES(state, payload) {
    state.roles_filter = payload
  }
}

export const actions = {
  async register({ }, payload) {
    return await this.$axios.$post("/auth/register", payload)
  },
  async login({ commit }, payload) {
    const response = await this.$axios.$post("/auth/login", payload)
    Cookie.set('token', response.token)
    commit('SET_LOGIN', true)
    commit('SET_TOKEN', response.token)
    commit('SET_USER', response.user)
  },

  async logout({ commit }) {
    await this.$axios.$get("/auth/logout")
    await commit('SET_LOGOUT');
    this.$router.push('/login')
  },

  async sendPasswordLink({ }, payload) {
    await this.$axios.post('/auth/password/forgot', payload)
  },

  async resetPassword({ }, payload) {
    await this.$axios.post('/auth/password/reset', payload)
  },

  async loadAuthUser({ commit }) {
    try {
      const response = await this.$axios.get('/user')
      commit('SET_USER', response.data.data)
    } catch (error) {
      await this.$axios.$get("/auth/logout")
      commit('SET_LOGOUT')
      return error
    }
  },

  async loadRoles({ commit }) {
    const response = await this.$axios.get('/auth/role')
    const rolesFilter = response.data.map((role) => {
      return {
        text: role.name,
        value: role.id,
      };
    });
    commit('SET_FILTER_ROLES', rolesFilter)
  }
}
