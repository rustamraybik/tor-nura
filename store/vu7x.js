export const state = () => ({
    count_unsign: 0,

})

export const getters = {
    getCountUnsign: (state) => {
        return state.count_unsign
    }
}

export const mutations = {
    SET_COUNT_UNSIGN(state, count) {
        state.count_unsign = count;
    },
}

export const actions = {
    async loadVu7x({ }, payload = {}) {
        const { page, count, search, type } = payload;
        return await this.$axios.get('/vu7x' +
            (Object.keys(payload).length ? '?' : '') +
            (type ? `&type=${type}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
    },

    async addWagonToVu78({ }, payload = {}) {
        const { vu78_id } = payload;
        return await this.$axios.post('/vu7x/vu78/' + (vu78_id ? `${vu78_id}/wagon/store` : 'store'), payload)
    },

    async addWagonToVu79({ }, payload = {}) {
        const { vu79_id } = payload;
        return await this.$axios.post('/vu7x/vu79/' + (vu79_id ? `${vu79_id}/wagon/store` : 'store'), payload)
    },

    async deleteWagon({ }, payload = {}) {
        const { vu7x_id, wagon_id, form_type } = payload;
        return await this.$axios.$delete(`/vu7x/${vu7x_id}/wagon/${wagon_id}${form_type ? `?form_type=${form_type}` : ''}`);
    },

    async deleteVu7x({ }, payload = {}) {
        const { vu78_id, form_type } = payload;
        return await this.$axios.$delete(`/vu7x/${vu78_id}${form_type ? `?form_type=${form_type}` : ''}`);
    },

    async signVu78({ }, payload = {}) {
        const { vu78_id, requestData, form_type } = payload;
        return await this.$axios.$post(`/vu7x/${vu78_id}/sign` +
            (form_type ? `?form_type=${form_type}` : ''), requestData);
    },

    async downloadVu7xDoc({ }, payload = {}) {
        const { vu78_id, file_id, format, isNotBlob, form_type } = payload;

        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/vu7x/${vu78_id}/document/${file_id}/download` +
            (form_type || format ? '?' : '') +
            (form_type ? `&form_type=${form_type}` : '') +
            (format ? `&format=${format}` : ''), option);
    },

    async loadWagonNum({ }, payload) {
        const { search, form_type } = payload;
        return await this.$axios.get('/vu7x/wagon_num' +
            (Object.keys(payload).length ? '?' : '') +
            (form_type ? `&form_type=${form_type}` : '') +
            (search ? `&search=${search}` : ''));
    },

    async updateWagon({ }, payload = {}) {
        const { id: wagon_id } = payload;
        await this.$axios.post(`/wagon/${wagon_id}/update`, payload);
    },

    async loadCountUnsign({ commit }) {
        const response = await this.$axios.get('/vu7x/count_unsign');
        commit('SET_COUNT_UNSIGN', response.data)
    },
}