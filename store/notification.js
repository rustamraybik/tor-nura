const getDefaultState = () => ({
    notification: {},
    notificationType: { data: [], meta: {} },
    notificBlockUsers: [],
    notificationGroup: [],
    notificationCount: 0,
})


export const state = getDefaultState;

export const getters = {
    getNotificationTypeList(state) {
        const result = Object.assign({}, state.notificationType);

        const data = state.notificationType.data.map((notific) => ({
            id: notific.id,
            body: notific.body,
            title: notific.title,
            format: notific.format,
            updated_at: notific.updated_at,
        }));

        result.data = data;
        return result;
    },

    getNotificBlockUsersList(state) {
        const result = { data: [], meta: {} };
        result.data = state.notificBlockUsers.map((user) => {
            const mapResult = {
                name: user.name,
                email: user.email,
                depot_name: user.depot_name,
                role_name: user.role_name
            };
            Object.defineProperty(mapResult, "id", {
                value: user.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "notific_id", {
                value: user.notific_id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "is_notific_show", {
                value: user.is_notific_show,
                enumerable: false,
            });
            return mapResult;
        });
        return result;
    },

    getNotificationGroup(state) {
        return state.notificationGroup;
    },

    getNotification(state) {
        return state.notification;
    },

    getCountNotification(state) {
        return state.notificationCount
    }
}

export const mutations = {
    RESET_STATE(state) {
        Object.assign(state, getDefaultState())
    },

    SET_NOTIFICATION_TYPE(state, payload) {
        state.notificationType = payload;
    },

    SET_NOTIFICATION_GROUP(state, payload) {
        state.notificationGroup = payload;
    },

    SET_NOTIFICATION(state, payload) {
        state.notification = payload;
    },

    SET_NOTIFICATION_COUNT(state, payload) {
        state.notificationCount = payload;
    },

    SET_NOTIFICATION_BLOCK_USERS(state, payload) {
        state.notificBlockUsers = payload;
    }
}

export const actions = {
    resetState({ commit }) {
        commit('RESET_STATE');
    },

    resetNotificBlockUsers({ commit }) {
        commit('SET_NOTIFICATION_TYPE', []);
    },

    async loadNotificationGroups({ commit }, payload = {}) {
        const response = await this.$axios.$get('/admin/notification/group');
        commit('SET_NOTIFICATION_GROUP', response);
    },

    async loadNotificationTypes({ commit }, payload = {}) {
        const { page, count, group_id } = payload;
        const response = await this.$axios.$get('/admin/notification/type' +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (group_id ? `&group_id=${group_id}` : ''));

        commit('SET_NOTIFICATION_TYPE', response);
    },

    async loadNotification({ commit }, payload = {}) {
        const { isPage } = payload;
        const response = await this.$axios.$get('/notification/' +
            (Object.keys(payload).length ? '?' : '') +
            (isPage ? `&is-page=${isPage}` : ''));
        commit('SET_NOTIFICATION', response.notifics);
        commit('SET_NOTIFICATION_COUNT', response.count_unread);
    },



    async markNotification({ }, payload = {}) {
        const { mark_id } = payload;
        await this.$axios.$get(`/notification/${mark_id}/mark`);
    },

    async markAllNotification() {
        await this.$axios.$get(`/notification/mark`);
    },

    async updateNotificationType({ }, payload = {}) {
        const { id } = payload;
        return await this.$axios.$post(`/admin/notification/type/${id}/update`, payload);
    },

    async loadNotificationBlockUser({ commit }, payload = {}) {
        const { id, search, status, role_id, depot_id } = payload;

        const response = await this.$axios.$get(`/admin/notification/type/${id}/block/user` +
            (Object.keys(payload).length > 1 ? '?' : '') +
            (search ? `&search=${search}` : '') +
            (depot_id !== null ? `&depot_id=${depot_id}` : '') +
            (status !== null ? `&status=${status}` : '') +
            (role_id !== null ? `&role_id=${role_id}` : ''));

        commit('SET_NOTIFICATION_BLOCK_USERS', response);
    },

    async blockNotification({ }, payload = {}) {
        const { id, user_id, is_notific_show } = payload;
        await this.$axios.$get(`/admin/notification/type/${id}/block/user/${user_id}` +
            (is_notific_show ? '?is_off=1' : ''));
    },

    async reverseBlockNotification({ }, payload = {}) {
        const { id, search, status, role_id, depot_id } = payload;

        await this.$axios.$get(`/admin/notification/type/${id}/block/user/reverse` +
            (Object.keys(payload).length > 1 ? '?' : '') +
            (search ? `&search=${search}` : '') +
            (depot_id !== null ? `&depot_id=${depot_id}` : '') +
            (status !== null ? `&status=${status}` : '') +
            (role_id !== null ? `&role_id=${role_id}` : ''));
    }
}
