export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
    async updateOrg({ }, payload = {}) {
        const { id } = payload
        return await this.$axios.post(`/organization/${id}/update`, payload)
    },
}
