const getDefaultState = () => ({
    wagons: {},
    bogie: {},
    wheels: {},
})

export const state = getDefaultState;

export const getters = {
    getPartLists: (state) => {
        const data = state.wagons.data?.map((item) => {
            const mapResult = {
                wagon_num: item.wagon.wagon_num,
                wagon_id: item.wagon.id,
                created_at: item.created_at,
                id: item.id,
                status: item.status,
                file_id: item.documents[0].file_id
            };
            return mapResult;
        });

        return data;
    },
    getBogie: (state) => {
        return state.bogie;
    },
    getWheels: (state) => {
        return state.wheels;
    },
}


export const mutations = {
    SET_WAGONS(state, payloads) {
        state.wagons = payloads;
    },
    SET_BOGIE(state, payloads) {
        state.bogie = payloads;
    },
    SET_WHEELS(state, payloads) {
        state.wheels = payloads;
    },
    resetState: (state) => {
        Object.assign(state, getDefaultState())
    },
}


export const actions = {
    resetState({ commit }) {
        commit('resetState');
    },

    async loadPartLists({ commit }, payload = {}) {
        const { search } = payload;
        const response = await this.$axios.get('/wagon/part_list' +
            (Object.keys(payload).length ? '?' : '') +
            (search ? `&search=${search}` : ''));
        commit('SET_WAGONS', response.data);
    },

    async loadEquip({ commit }, payload = {}) {
        const { page, count, search, id, is_wheel } = payload;
        const response = await this.$axios.get(`/wagon/${id}/part` +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (is_wheel ? `&is_wheel=${is_wheel}` : '') +
            (search ? `&search=${search}` : ''));
        if (is_wheel) {
            commit('SET_WHEELS', response.data);
        } else {
            commit('SET_BOGIE', response.data);
        }
    },

    async addEquip({ }, payload = {}) {
        const { id, data } = payload;
        await this.$axios.post(`/wagon/${id}/part/store`, data);
    },

    async sendPartList({ }, payload = {}) {
        const { id } = payload;
        await this.$axios.get(`/wagon/${id}/part_list/accept`);
    },

    async declinePartList({ }, payload = {}) {
        const { id } = payload;
        await this.$axios.get(`/wagon/${id}/part_list/decline`);
    },

    async downloadPartListDoc({ }, payload = {}) {
        const { id, file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/wagon/${id}/part_list/document/${file_id}/download` +
            (format ? `?format=${format}` : ''), option);
    },

    async editEquip({ }, payload = {}) {
        const { wagonId, partId, data } = payload;
        await this.$axios.post(`/wagon/${wagonId}/part/${partId}/update`, data);
    },

    async deleteEquip({ }, payload = {}) {
        const { wagonId, partId } = payload;
        await this.$axios.delete(`/wagon/${wagonId}/part/${partId}/delete`);
    }
}