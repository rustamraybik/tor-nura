export const state = () => ({

})

export const getters = {

}

export const mutations = {

}

export const actions = {
    async loadVu8x({ }, payload = {}) {
        const { page, count, search, form_type } = payload;
        return await this.$axios.get('/vu8x' +
            (Object.keys(payload).length ? '?' : '') +
            (form_type ? `&form_type=${form_type}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
    },

    async createVu8x({ }, payload = {}) {
        const { vu7x_id, form_type } = payload
        return await this.$axios.get('/vu8x/store' +
            (Object.keys(payload).length ? '?' : '') +
            (vu7x_id ? `&vu7x_id=${vu7x_id}` : '') +
            (form_type ? `&form_type=${form_type}` : ''))
    },


    async downloadVu8xDoc({ }, payload = {}) {
        const { vu8x_id, file_id, format, isNotBlob, form_type } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/vu8x/${vu8x_id}/document/${file_id}/download` +
            (Object.keys(payload).length ? '?' : '') +
            (format ? `&format=${format}` : '') +
            (form_type ? `&form_type=${form_type}` : ''), option);
    },

    async signVu8x({ }, payload = {}) {
        const { vu8x_id, requestData, form_type } = payload;
        return await this.$axios.$post(`/vu8x/${vu8x_id}/sign?form_type=${form_type}`, requestData);
    },

    async deleteVu8x({ }, payload = {}) {
        const { vu8x_id, form_type } = payload;
        return await this.$axios.$delete(`/vu8x/${vu8x_id}?form_type=${form_type}`);
    }

}