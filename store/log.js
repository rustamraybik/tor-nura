export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
    async loadLogs({ }, payload = {}) {
        const { page, count, search, role_id } = payload;
        return await this.$axios.get('/admin/log' +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (role_id ? `&role_id=${role_id}` : '') +
            (search ? `&search=${search}` : '')
        )
    },
    async loadFullRequest({ }, payload = {}) {
        const { logId } = payload;
        return await this.$axios.$get(`/admin/log/${logId}`);
    },
    async downloadLogsExcel({ }, payload = {}) {
        return await this.$axios.$get(`/admin/log/download`,
            {
                responseType: "blob",
            });
    },
}
