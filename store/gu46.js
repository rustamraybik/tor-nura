const getDefaultState = () => ({
    wagons: {},
    gu46s: {},
    altDate: "Не указано",
})

export const state = getDefaultState;


export const getters = {
    getListGu46sForModal: (state) => {
        const result = Object.assign({}, state.gu46s);
        const data = state.gu46s.data?.filter(gu46 => !gu46.closing_date).map((item) => {
            const mapResult = {
                id: item.id,
                created_at: item.created_at,
                wagonsCount: item.wagons.length,
            };
            return mapResult;
        });



        result.data = data;
        return result;
    },
    getListGu46sForTable: (state) => {
        const result = Object.assign({}, state.gu46s);
        const data = state.gu46s.data.map((item) => {
            const mapResult = {
                id: item.id,
                created_at: item.created_at,
                countWagons: item.wagons.length,
                closing_date: item.closing_date || "btn",
            };
            Object.defineProperty(mapResult, "file_id", {
                value: item.documents[0].file_id,
                enumerable: false,
            });
            return mapResult;
        });

        result.data = data;
        return result;
    },
    getListWagons: (state) => {
        const result = Object.assign({}, state.wagons);
        const data = state.wagons.data?.map((item) => {
            const mapResult = {
                wagon_num: item.wagon_num,
                owner_name: item.owner_name,
                vu23_at: item.vu23?.last_sign_date || state.altDate,
                vu80_at: item.vu80?.last_sign_date || state.altDate,
                vu36_at: item.vu36?.last_sign_date || state.altDate,
                vu81_at: item.vu81?.last_sign_date || state.altDate,
                gu46_id: item.gu46?.id || '',
                gu46_at: item.gu46?.last_sign_date || "btn",
            };
            Object.defineProperty(mapResult, "id", {
                value: item.id,
                enumerable: false,
            });
            return mapResult;
        });

        result.data = data;
        return result;
    },
    getListWagonsForSubTable: (state) => {
        const data = state.gu46s.data.map((item) => {
            let data = item.wagons.map((wagon) => {
                const mapResult = {
                    wagon_num: wagon.wagon_num,
                    owner_name: wagon.owner_name || "Не указан",
                    vu23: wagon.vu23?.last_sign_date || "",
                    vu80: wagon.vu80?.last_sign_date || "",
                    vu36: wagon.vu36?.last_sign_date || "",
                    vu81: wagon.vu81?.last_sign_date || "",
                };
                Object.defineProperty(mapResult, "id", {
                    value: wagon.id,
                    enumerable: false,
                });
                return mapResult;
            })

            const isCloseable = item.wagons.every((wagon) => {
                let hasSign = true;
                for (const key in wagon) {
                    if (['vu23', 'vu80', 'vu36', 'vu81'].includes(key)) {
                        hasSign = !!wagon[key]?.last_sign_date && hasSign;
                    }
                }
                return hasSign;
            })
            return { data, isCloseable }
        });

        return data;
    },
    getAltDate: (state) => {
        return state.altDate;
    },
}


export const mutations = {
    SET_WAGONS(state, payloads) {
        state.wagons = payloads
    },
    SET_GU46S(state, payloads) {
        state.gu46s = payloads
    },
    resetState: (state) => {
        Object.assign(state, getDefaultState())
    },
}


export const actions = {
    resetState({ commit }) {
        commit('resetState');
    },

    async loadGu46s({ commit }, payload = {}) {
        const { page, count, search, left_date, right_date } = payload;
        const response = await this.$axios.get('/gu46' +
            (Object.keys(payload).length ? '?' : '') +
            (left_date ? `&left_date=${left_date}` : '') +
            (right_date ? `&right_date=${right_date}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
        commit('SET_GU46S', response.data)
    },

    async loadWagons({ commit }, payload = {}) {
        const { page, count, search, owner_name, left_date, right_date, form_type, order_by } = payload;
        const response = await this.$axios.get('/gu46/wagon' +
            (Object.keys(payload).length ? '?' : '') +
            (owner_name ? `&owner_name=${owner_name}` : '') +
            (left_date ? `&left_date=${left_date}` : '') +
            (right_date ? `&right_date=${right_date}` : '') +
            (form_type ? `&form_type=${form_type}` : '') +
            (order_by ? `&order_by=${order_by}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
        commit('SET_WAGONS', response.data)
    },

    async addWagonToGu46({ }, payload = {}) {
        const { gu46_id } = payload;
        return await this.$axios.post('/gu46/' + (gu46_id ? `${gu46_id}/wagon/store` : 'store'), payload)
    },

    async getWagonOwner({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/gu46/wagon/owner' +
            (search ? `?search=${search}` : '')
        )
    },

    async deleteWagon({ }, payload = {}) {
        const { gu46_id, wagon_id } = payload;
        return await this.$axios.$delete(`/gu46/${gu46_id}/wagon/${wagon_id}`);
    },

    async deleteGu46({ }, payload = {}) {
        const { id } = payload;
        return await this.$axios.$delete(`/gu46/${id}`);
    },

    async downloadGu46Doc({ }, payload = {}) {
        const { gu46_id, file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/gu46/${gu46_id}/document/${file_id}/download` +
            (format ? `?format=${format}` : ''), option);
    },

    async acceptGu46({ }, payload = {}) {
        const { gu46_id } = payload;
        return await this.$axios.$get(`/gu46/${gu46_id}/accept`);
    },
}