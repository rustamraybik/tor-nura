const getDefaultState = () => ({
    wagon: {},
    vu22: {},
    operation: [],
})

export const state = getDefaultState;


export const getters = {
    getVu22List: (state) => {
        const data = state.vu22.data?.map((item) => {
            const mapResult = {
                wagon_num: item.wagon.wagon_num,
                wagon_id: item.wagon.id,
                chief_name: item.depot.chief_name,
                lifting_capacity: item.lifting_capacity,
                is_metal: item.is_metal,
                id: item.id,
                operations: item.operations,
                file_id: item.documents[0].file_id,
                created_at: item.created_at
            };
            Object.defineProperty(mapResult, "documents", {
                value: item.documents,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "signs", {
                value: item.signs,
                enumerable: false,
            });
            return mapResult;
        });
        return data;
    },

    getWagon: (state) => {
        let result = {
            ...state.wagon?.last_repair,
            ...state.wagon,
            repair_type_id: state.wagon?.last_repair?.repair_type.id,
            depot_owner: state.wagon?.depot?.chief_name,
        }
        return result;
    },

    getOperation: (state) => {
        return state.operation.data;
    },
}


export const mutations = {
    SET_VU22(state, payloads) {
        state.vu22 = payloads;
    },
    SET_WAGON(state, payloads) {
        state.wagon = payloads;
    },
    SET_OPERATION(state, payloads) {
        state.operation = payloads;
    },
    RESET_WAGON(state) {
        state.wagon = {};
    },
}

export const actions = {
    resetWagon({ commit }) {
        commit('RESET_WAGON');
    },

    async loadVu22({ commit }, payload = {}) {
        const { search } = payload;
        const responseVu22 = await this.$axios.get('/vu22' +
            (search ? `?search=${search}` : ''));
        commit('SET_VU22', responseVu22.data);
    },

    async editWagon({ }, payload = {}) {
        const { id: wagon_id } = payload;
        await this.$axios.post(`/wagon/${wagon_id}/update`, payload);
    },

    async storeLastRepair({ }, payload = {}) {
        const { id: wagon_id } = payload;

        if (payload.repair_end_date) {
            var userTimezoneOffset = payload.repair_start_date.getTimezoneOffset() * 60000;
            payload.repair_end_date = new Date(payload.repair_end_date.getTime() - userTimezoneOffset)
        }

        if (payload.repair_start_date) {
            var userTimezoneOffset = payload.repair_start_date.getTimezoneOffset() * 60000;
            payload.repair_start_date = new Date(payload.repair_start_date.getTime() - userTimezoneOffset)
        }

        await this.$axios.post(`/wagon/${wagon_id}/last_repair/store`, payload);
    },

    async updateLastRepair({ }, payload = {}) {
        const { id: wagon_id } = payload;

        if (payload.repair_end_date) {
            var userTimezoneOffset = payload.repair_start_date.getTimezoneOffset() * 60000;
            payload.repair_end_date = new Date(payload.repair_end_date.getTime() - userTimezoneOffset)
        }

        if (payload.repair_start_date) {
            var userTimezoneOffset = payload.repair_start_date.getTimezoneOffset() * 60000;
            payload.repair_start_date = new Date(payload.repair_start_date.getTime() - userTimezoneOffset)
        }

        await this.$axios.post(`/wagon/${wagon_id}/last_repair/update`, payload);
    },

    async storeVu22({ commit }, payload = {}) {
        const { id: wagon_id, operations } = payload;
        const responseVu22 = await this.$axios.post(`/vu22/store`, { wagon_id, operations });
        return responseVu22;
    },

    async deleteVu22({ }, payload = {}) {
        const { vu22_id } = payload;
        await this.$axios.delete(`/vu22/${vu22_id}`);
    },

    async storeOperation({ }, payload = {}) {
        const { vu22_id, data } = payload;
        await this.$axios.post(`/vu22/${vu22_id}/operation/store`, data);
    },

    async updateOperation({ }, payload = {}) {
        const { vu22_id, vu22_operation_id, data } = payload;
        await this.$axios.post(`/vu22/${vu22_id}/operation/${vu22_operation_id}/update`, data);
    },

    async deleteOperation({ }, payload = {}) {
        const { vu22_id, vu22_operation_id } = payload;
        await this.$axios.delete(`/vu22/${vu22_id}/operation/${vu22_operation_id}`);
    },

    async loadWagonNum({ }, payload) {
        const { search } = payload;
        return await this.$axios.get('/vu22/wagon_num' +
            (Object.keys(payload).length ? '?' : '') +
            (search ? `&search=${search}` : ''));
    },

    async loadWagon({ commit }, payload = {}) {
        const { id: wagon_id } = payload;
        const response = await this.$axios.get(`/wagon/${wagon_id}/`);
        commit('SET_WAGON', response.data);
    },

    async operation({ commit }, payload = {}) {
        const { search, group_id } = payload;
        const response = await this.$axios.get(`/vu22/operation/`
            + (group_id ? `?group_id=${group_id}` : '')
            + (search ? `&search=${search}` : ''));
        commit('SET_OPERATION', response.data);
        return response.data
    },

    async downloadVu22Doc({ }, payload = {}) {
        const { vu22_id, file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/vu22/${vu22_id}/document/${file_id}/download` +
            (format ? `?format=${format}` : ''), option);
    },

    async signVu22({ }, payload = {}) {
        const { vu22_id, requestData } = payload;
        return await this.$axios.$post(`/vu22/${vu22_id}/sign`, requestData);
    },

    async declineVu22({ }, payload = {}) {
        const { vu22_id } = payload;
        return await this.$axios.$post(`/vu22/${vu22_id}/decline`);
    }
}