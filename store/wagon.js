export const state = () => ({
    currentCustomerWagon: {},
    currentCustomerWagonError: 'Укажите номер вагона в поиске',
    userWagons: null,
});

export const getters = {
    getCurrentCustomerWagon: (state) => {
        return state.currentCustomerWagon;
    },
    getError: (state) => {
        return state.currentCustomerWagonError;
    },
    getUserWagons: (state) => {
        return state.userWagons;
    },
}

export const mutations = {
    SET_CURRENT_WAGON(state, { data }) {
        state.currentCustomerWagon = data;
    },
    SET_CURRENT_WAGON_ERROR(state, errorMessage) {
        state.currentCustomerWagonError = errorMessage;
    },
    SET_USER_WAGONS(state, data) {
        state.userWagons = data;
    }
}

export const actions = {
    async loadWagons({ }, payload = {}) {
        const { page, count, search, station_id, depot_id } = payload;
        return await this.$axios.get('/admin/wagon' +
            (Object.keys(payload).length ? '?' : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : '') +
            (depot_id ? `&depot_id=${depot_id}` : '') +
            (station_id ? `&station_id=${station_id}` : ''));
    },

    async showWagon({ }, payload = {}) {
        const { wagon_id } = payload;
        return await this.$axios.get(`wagon/${wagon_id}`)
    },

    async showWagonByNum({ commit }, payload) {
        try {
            const wagon = await this.$axios.get(`wagon/get/${payload}`);
            commit('SET_CURRENT_WAGON', wagon);
        } catch (e) {
            commit('SET_CURRENT_WAGON_ERROR', e.response.data.errorMessage)
        }
    },

    async dropCurrentWagon({ commit }) {
        commit('SET_CURRENT_WAGON', { data: {} });
    },

    async dropCurrentWagonError({ commit }) {
        commit('SET_CURRENT_WAGON_ERROR', 'Укажите номер вагона в поиске');
    },

    async addWagonToUser({ commit }, payload) {
        const { data } = await this.$axios.put(`wagon/addUser/${payload}`);
        commit('SET_CURRENT_WAGON', await data);
    },

    async loadUserWagons({ commit }, payload) {
        const { 
            page = 1,
            count = 10,
            search,
            vu23Status,
            applicationStatus,
            date,
        } = payload;
        const { data } = await this.$axios.get(`wagon/getUserWagons?` +
            `page=${page}` +
            `&count=${count}` +
            (search ? `&search=${search}` : '') +
            (vu23Status ? `&vu23Status=${vu23Status}` : '') +
            (applicationStatus ? `&applicationStatus=${applicationStatus}` : '') +
            (date ? `&date=${date}` : '')
        );
        commit('SET_USER_WAGONS', await data);
        return;
    },

    async downloadWagonsExcel({ }, payload = {}) {
        const { search, depot_id } = payload;
        return await this.$axios.$get(`/admin/wagon/download` +
            (Object.keys(payload).length ? '?' : '') +
            (search ? `&search=${search}` : '') +
            (depot_id ? `&depot_id=${depot_id}` : ''),
            {
                responseType: "blob",
            });
    },

    async uploadApplication({ commit }, { file, vu23Id }) {
        const formData = new FormData();
        formData.append('file', file, file.name);
        const data = await this.$axios.$post(
            `/vu23/${vu23Id}/uploadApplication`,
            formData
        );
        commit('SET_USER_WAGONS', await data);
    },

    async downloadApplication({}, vu23Id) {
        return await this.$axios.$get(`/vu23/${vu23Id}/downloadApplication`, {responseType: "blob"});
    },

    async deleteApplication({ commit }, vu23Id) {
        const data = await this.$axios.$delete(
            `/vu23/${vu23Id}/deleteApplication`
        );
        commit('SET_USER_WAGONS', await data);
    },
}