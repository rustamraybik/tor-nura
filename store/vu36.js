const getDefaultState = () => ({
    wagons: {},
    vu36s: {},
    altDate: "Не указано",
})

export const state = getDefaultState;

export const getters = {
    getListVu36sForModal: (state) => {
        const result = Object.assign({}, state.vu36s);
        const data = state.vu36s.data?.filter(vu36 => vu36.signs.length === 0).map((item) => {
            const mapResult = {
                id: item.id,
                created_at: item.created_at,
                wagonsCount: item.wagons.length,
            };
            return mapResult;
        });

        result.data = data;
        return result;
    },

    getListVu36sForTable: (state) => {
        const result = Object.assign({}, state.vu36s);
        const data = state.vu36s.data.map((item) => {
            const mapResult = {
                id: item.id,
                created_at: item.created_at,
                countWagons: item.wagons.length,
                signBtn: "btn",
            };

            Object.defineProperty(mapResult, "file_id", {
                value: item.documents[0].file_id,
                enumerable: false,
            });

            Object.defineProperty(mapResult, "documents", {
                value: item.documents,
                enumerable: false,
            });

            Object.defineProperty(mapResult, "signs", {
                value: item.signs,
                enumerable: false,
            });
            return mapResult;
        });

        result.data = data;
        return result;
    },

    getListWagons: (state) => {
        const result = Object.assign({}, state.wagons);
        const data = state.wagons?.data.map((item) => {
            const mapResult = {
                wagon_num: item.wagon_num,
                park_number: item.home_road || '-',
                type_name: item.type.name,
                specifications: '-',
                vu80_at: item.vu80?.last_sign_date || state.altDate,
                vu36_at: item.vu36?.last_sign_date || "btn",
            };
            Object.defineProperty(mapResult, "id", {
                value: item.id,
                enumerable: false,
            });
            Object.defineProperty(mapResult, "vu36_id", {
                value: item.vu36?.id,
                enumerable: false,
            });
            return mapResult;
        });

        result.data = data;
        return result;
    },

    getListWagonsForSubTable: (state) => {
        const data = state.vu36s.data.map((item) =>
            item.wagons.map((wagon) => {
                const mapResult = {
                    wagon_num: wagon.wagon_num,
                    home_road: wagon.home_road || '-',
                    type_name: wagon.type.name,
                    specifications: '-',
                    vu80: wagon.vu80?.last_sign_date || "",
                };
                Object.defineProperty(mapResult, "id", {
                    value: wagon.id,
                    enumerable: false,
                });
                return mapResult;
            })
        );

        return data;
    },

    getAltDate: (state) => {
        return state.altDate;
    },
}


export const mutations = {
    SET_VU36_WAGONS(state, payloads) {
        state.wagons = payloads
    },

    SET_VU36S(state, payloads) {
        state.vu36s = payloads
    },

    resetState: (state) => {
        Object.assign(state, getDefaultState())
    },
}


export const actions = {
    resetState({ commit }) {
        commit('resetState');
    },

    async loadVu36s({ commit }, payload = {}) {
        const { page, count, search, left_date, right_date } = payload;
        const response = await this.$axios.get('/vu36' +
            (Object.keys(payload).length ? '?' : '') +
            (left_date ? `&left_date=${left_date}` : '') +
            (right_date ? `&right_date=${right_date}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
        commit('SET_VU36S', response.data)
    },

    async loadWagons({ commit }, payload = {}) {
        const { page, count, search, left_date, right_date, wagon_type } = payload;
        const response = await this.$axios.get('/vu36/wagon' +
            (Object.keys(payload).length ? '?' : '') +
            (left_date ? `&left_date=${left_date}` : '') +
            (right_date ? `&right_date=${right_date}` : '') +
            (wagon_type ? `&wagon_type=${wagon_type}` : '') +
            (page ? `&page=${page}` : '') +
            (count ? `&count=${count}` : '') +
            (search ? `&search=${search}` : ''));
        commit('SET_VU36_WAGONS', response.data);
    },

    async addWagonToVu36({ }, payload = {}) {
        const { vu36_id } = payload;
        return await this.$axios.post('/vu36/' + (vu36_id ? `${vu36_id}/wagon/store` : 'store'), payload)
    },

    async getWagonOwner({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/vu36/wagon/owner' +
            (search ? `?search=${search}` : '')
        )
    },

    async deleteWagon({ }, payload = {}) {
        const { vu36_id, wagon_id } = payload;
        return await this.$axios.$delete(`/vu36/${vu36_id}/wagon/${wagon_id}`);
    },

    async deleteVu36({ }, payload = {}) {
        const { id } = payload;
        return await this.$axios.$delete(`/vu36/${id}`);
    },

    async downloadVu36Doc({ }, payload = {}) {
        const { vu36_id, file_id, format, isNotBlob } = payload;
        const option = isNotBlob ? null : {
            responseType: "blob",
        };
        return await this.$axios.$get(`/vu36/${vu36_id}/document/${file_id}/download` +
            (format ? `?format=${format}` : ''), option);
    },

    async signVu36({ }, payload = {}) {
        const { vu36_id, requestData } = payload;
        return await this.$axios.$post(`/vu36/${vu36_id}/sign`, requestData);
    },

    async declineVu36({ }, payload = {}) {
        const { vu36_id } = payload;
        return await this.$axios.$post(`/vu36/${vu36_id}/decline`);
    },
}