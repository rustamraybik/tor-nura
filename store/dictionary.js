export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
    async loadStations({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/dictionary/station' +
            (search ? `?search=${search}` : '')
        )
    },
    async loadDepots({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/dictionary/depot' +
            (search ? `?search=${search}` : '')
        )
    },
    async loadRailways({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/dictionary/railway' +
            (search ? `?search=${search}` : '')
        )
    },
    async loadWagonTypes({ }, payload = {}) {
        const { search } = payload;
        return await this.$axios.get('/dictionary/wagon-type' +
            (search ? `?search=${search}` : '')
        )
    },
}
