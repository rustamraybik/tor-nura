export default function ({store, redirect}) {
  if (!store.getters['auth/getUserLogged']) {
    return redirect('/login')
  }
}
