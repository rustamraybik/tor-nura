export default function ({ redirect, store }) {
  if (store.getters['auth/getUserLogged']) {
    return redirect('/lk/personal')
  }
}
