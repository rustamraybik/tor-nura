export const scroll =  async function (refName) {
    const element = this.$refs[refName].$el.children[2];
    const top = element.offsetTop + 363;
    let count = 100;

    let start_top = window.scrollY;

    let delta = (top - window.scrollY) / 100;

    for (let i = 0; i < 100; i++) {
      await new Promise((resolve) => {
        window.setTimeout(function () {
          resolve();
        }, 1000 / count);
      });
      window.scrollTo(0, start_top + delta);
      start_top += delta;
    }
  }