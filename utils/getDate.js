export const getCurrentDate = () => {
    let d = (new Date()).toISOString().split('T');
    d[1] = d[1].split(':').slice(0,2).join(':');

    return d.join(' ');
  }