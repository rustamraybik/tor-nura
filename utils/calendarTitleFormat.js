export const titleDate = (dates) => {
    return dates
      .map((n) =>
        new Date(n).toLocaleString("ru-RU", {
          year: "2-digit",
          month: "numeric",
          day: "numeric",
        })
      )
      .join(" - ");
  }