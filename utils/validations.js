export const isNumber = (event) => {
    event = event ? event : window.event;
    let charCode = event.which ? event.which : event.keyCode;
    if (
        charCode > 31 &&
        (charCode < 48 || charCode > 57) &&
        charCode !== 46
    ) {
        event.preventDefault();
        return false;
    } else {
        return true;
    }
}

export const isLetter = (e) => {
    let paste = e.clipboardData?.getData("text");
    let char = paste || String.fromCharCode(e.keyCode);
    if (
      /^[а-яА-ЯёЁa-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/.test(
        char
      )
    ) {
      return true;
    } else {
      e.preventDefault();
    }
  }