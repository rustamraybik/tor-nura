const cookieparser = process.server ? require('cookieparser') : undefined
const Cookie = process.client ? require('js-cookie') : undefined

export default function ({ $axios, error: nuxtError, req, store, redirect }) {
  let token = null
  if (req && req.headers.cookie) {
    const parsed = cookieparser.parse(req.headers.cookie)
    token = parsed.token
  }
  if (process.client) {
    token = Cookie.get('token')
  }
  $axios.setToken(token, 'Bearer')
  $axios.onRequest(config => {
    if (process.client && !config.headers['Authorization']) {
      config.headers['Authorization'] = 'Bearer ' + store.state.auth.token
    }
  })

  $axios.onError(error => {
    if (error.response.status === 401) {
      nuxtError({
        statusCode: error.response.status,
        message: error.message,
      });
      return Promise.resolve(false);
    }
  })
}
