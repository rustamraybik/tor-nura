import Vue from 'vue'
import VueLayers from 'vuelayers'
import 'vuelayers/lib/style.css'
import { getFeatureId } from 'vuelayers/lib/ol-ext'

Vue.use(VueLayers);

window.getFeatureId = getFeatureId;
