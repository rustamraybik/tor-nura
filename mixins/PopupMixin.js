export const PopupMixin = {
    methods: {
        changeExtra(index) {
            if (index === this.openExtraIdx) {
              this.openExtraIdx = null;
            } else {
              this.openExtraIdx = index;
            }
          },
        closeExtra(event) {
            if (!event.target.className.includes("extra-btn")) {
              this.openExtraIdx = null;
            }
          },
    }
}