export const DialogModalMixin = {
    methods: {
        changeModal(modalName, status = true) {
            this.$refs[modalName].isOpen = status;
          },
    }
}