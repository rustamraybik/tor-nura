import { NCALayerClient } from "ncalayer-js-client";

export default async function ( documentBase64 ) {
    const ncalayerClient = new NCALayerClient();
    try {
      await ncalayerClient.connect();
    } catch (error) {
      alert(`Не удалось подключиться к NCALayer: ${error.toString()}`);
      throw error;
    }
    let activeTokens;
    try {
      activeTokens = await ncalayerClient.getActiveTokens();
    } catch (error) {
      alert(error.toString());
      throw error;
    }
    const storageType = activeTokens[0] || NCALayerClient.fileStorageType;
    let base64EncodedSignature;
    try {
      base64EncodedSignature =
        await ncalayerClient.createCAdESFromBase64(
          storageType,
          documentBase64
        );
    } catch (error) {
      throw error;
    }
    return base64EncodedSignature;
  }
  