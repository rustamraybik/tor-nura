export const roles = {
    ADMIN: 1,
    WAGON_OPERATOR: 2,
    TOR_MASTER: 3,
    STATION_OFFICER: 4,
    CARGO_RECEIVER: 5,
    RAILWAY_EMPLOYEE: 6,
    REPAIR_CUSTOMER: 7,
};

export const docType = {
    MAIN_FORM: 1,
}

export const userDocTypes = {
    TEST_SIGN: 1,
    CONFIRM_DOCUMENT: 2
}

export const partListStatus = {
    WAITING_MASTER_TOR: 1,
    WAITING_WAGON_OPERATOR: 2,
    AGREED: 3
}

export const partParameter = {
    SERIAL_NUMBER: 1,
    MFR_CODE: 2,
    ISSUE_YEAR: 3,
    SURVEY_CODE: 4,
    SURVEY_YEAR: 5,
    SURVEY_MOUNTH: 6,
    RIGHT_RIM_THICKNESS: 7,
    LEFT_RIM_THICKNESS: 8,
    OWNER_CODE: 9,
}

export const typeWorks = {
    CHASSIS: {name: "Ходовая часть", group_id: 1},
    SPRING_SUSPENSION: {name: "Рессорное подвешивание", group_id: 2},
    AXLEBOXES: {name: "Буксы и подшипники", group_id: 3},
    AUTOCOUPLING: {name: "Автосцепное устройство", group_id: 4},
    SPARE_PARTS: {name: "Метал. Части кузова и рамы вагона", group_id: 5},
    WELDING_WORKS: {name: "Сварочные работы", group_id: 6},
    BRAKING_EQUIP: {name: "Тормозное оборудование", group_id: 7},
}